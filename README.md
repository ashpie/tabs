# Tabs

Tabs is an electron app that lets you easily turn any webpage into a "desktop app". We call each of this "apps" `Services`.

You can add multiple Services to a single window and easily switch between them with the navigation bar, like persistent tabs in a web browser.

You can navigate inside the focused service using the navigation buttons (previous page, next page).

## Contributing

## License
[MIT - see LICENSE file](LICENSE)